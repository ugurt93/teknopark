/* -----------------------------------------------------------------------------

  jQuery DateTimePicker - Responsive flat design jQuery DateTime Picker plugin for Web & Mobile
  Version 0.1.38
  Copyright (c)2017 Lajpat Shah
  Contributors : https://github.com/nehakadam/DateTimePicker/contributors
  Repository : https://github.com/nehakadam/DateTimePicker
  Documentation : https://nehakadam.github.io/DateTimePicker

 ----------------------------------------------------------------------------- */

/*

	language: English
	file: DateTimePicker-i18n-en

*/

(function ($) {
    $.DateTimePicker.i18n["tr"] = $.extend($.DateTimePicker.i18n["tr"], {

    language: "tr",

    dateTimeFormat: "dd-MM-yyyy HH:mm",
		dateFormat: "dd-MM-yyyy",
		timeFormat: "HH:mm",

    shortDayNames:["Paz", "Pzt", "Sal", "Çar", "Per", "Cum", "Cumt"],
    fullDayNames:["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
    shortMonthNames:["Oca", "Şub", "Mar", "Nis", "May", "Haz", "Tem","Ağu","Eyl","Eki","Kas","Ara"],
    fullMonthNames:["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz","Ağustos","Eylül","Ekim","Kasım","Aralık"],

		titleContentDate:"Lütfen Tarih Seçiniz",
		titleContentTime: "Lütfen Saat Seçiniz",
		titleContentDateTime: "Lütfen Tarih ve Saat Seçiniz",

		setButtonContent:"Tarihi Seç",
		clearButtonContent: "İptal"

    });
})(jQuery);
