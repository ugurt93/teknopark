package teknopark.models.forms;

import teknopark.models.entities.TextItem;

public class FaqElement {
    private String section;
    private String heading;
    private TextItem title;
    private TextItem text;
    private TextItem color;

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public TextItem getTitle() {
        return title;
    }

    public void setTitle(TextItem title) {
        this.title = title;
    }

    public TextItem getText() {
        return text;
    }

    public void setText(TextItem text) {
        this.text = text;
    }

    public TextItem getColor() {
        return color;
    }

    public void setColor(TextItem color) {
        this.color = color;
    }
}
