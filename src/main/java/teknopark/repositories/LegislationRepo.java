package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.Legislation;

import java.util.List;

@Repository
public interface LegislationRepo extends JpaRepository<Legislation,Long> {
    @Query("SELECT l FROM Legislation l where l.isDeleted = false")
    List<Legislation> findAll();
}
