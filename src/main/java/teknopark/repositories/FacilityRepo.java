package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.Facility;

import java.util.List;

@Repository
public interface FacilityRepo extends JpaRepository<Facility,Long> {
    @Query("SELECT f FROM Facility f where f.isDeleted = false")
    List<Facility> findAll();
}
