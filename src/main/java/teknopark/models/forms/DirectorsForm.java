package teknopark.models.forms;

import teknopark.models.entities.TextItem;

import java.util.List;

public class DirectorsForm {
    private AbstractHomeForm staticForm;

    private List<DirectorElement> directors;

    private TextItem directorHeader;
    private TextItem directorDescription;
    private TextItem directorItemHeader;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }

    public List<DirectorElement> getDirectors() {
        return directors;
    }

    public void setDirectors(List<DirectorElement> directors) {
        this.directors = directors;
    }

    public TextItem getDirectorHeader() {
        return directorHeader;
    }

    public void setDirectorHeader(TextItem directorHeader) {
        this.directorHeader = directorHeader;
    }

    public TextItem getDirectorDescription() {
        return directorDescription;
    }

    public void setDirectorDescription(TextItem directorDescription) {
        this.directorDescription = directorDescription;
    }

    public TextItem getDirectorItemHeader() {
        return directorItemHeader;
    }

    public void setDirectorItemHeader(TextItem directorItemHeader) {
        this.directorItemHeader = directorItemHeader;
    }
}
