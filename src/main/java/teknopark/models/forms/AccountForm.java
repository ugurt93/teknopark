package teknopark.models.forms;

import teknopark.models.entities.PhotoItem;

public class AccountForm {
    private PhotoItem photo;

    public PhotoItem getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoItem photo) {
        this.photo = photo;
    }
}
