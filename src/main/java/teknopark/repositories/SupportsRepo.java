package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.Support;

import java.util.List;

@Repository
public interface SupportsRepo extends JpaRepository<Support,Long> {
    @Query("SELECT s FROM Support s where s.isDeleted = false")
    List<Support> findAll();
}
