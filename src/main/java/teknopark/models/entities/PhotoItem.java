package teknopark.models.entities;

import javax.persistence.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;

@Entity
@Table(name = "photo_item")
public class PhotoItem implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "photo_tr")
    private String photoTr;

    @Column(name = "photo_en")
    private String photoEn;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Transient
    private String photo;

    @Transient
    private byte[] imgTr;

    @Transient
    private byte[] imgEn;


    public PhotoItem() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoTr() {
        return photoTr;
    }

    public void setPhotoTr(String photoTr) {
        this.photoTr = photoTr;
    }

    public String getphotoEn() {
        return photoEn;
    }

    public void setphotoEn(String photoEn) {
        this.photoEn = photoEn;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setLang(String lang) {
        if (lang.equals("tr"))
            photo = photoTr;
        else if (lang.equals("en"))
            photo = photoEn;

    }

    public byte[] getImgTr() {
        return imgTr;
    }

    public void setImgTr(byte[] imgTr) {
        this.imgTr = imgTr;
    }

    public byte[] getImgEn() {
        return imgEn;
    }

    public void setImgEn(byte[] imgEn) {
        this.imgEn = imgEn;
    }
}
