package teknopark.models.forms;

import teknopark.models.entities.PhotoItem;
import teknopark.models.entities.TextItem;

import java.util.List;

public class NewsForm {
    private AbstractHomeForm staticForm;

    private List<NewsElement> newsElements;
    private TextItem newsHeader;
    private TextItem newsDescription;
    private TextItem newsItemHeader;
    private TextItem newsView;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }

    public List<NewsElement> getNewsElements() {
        return newsElements;
    }

    public void setNewsElements(List<NewsElement> newsElements) {
        this.newsElements = newsElements;
    }

    public TextItem getNewsHeader() {
        return newsHeader;
    }

    public void setNewsHeader(TextItem newsHeader) {
        this.newsHeader = newsHeader;
    }

    public TextItem getNewsDescription() {
        return newsDescription;
    }

    public void setNewsDescription(TextItem newsDescription) {
        this.newsDescription = newsDescription;
    }

    public TextItem getNewsItemHeader() {
        return newsItemHeader;
    }

    public void setNewsItemHeader(TextItem newsItemHeader) {
        this.newsItemHeader = newsItemHeader;
    }

    public TextItem getNewsView() {
        return newsView;
    }

    public void setNewsView(TextItem newsView) {
        this.newsView = newsView;
    }
}
