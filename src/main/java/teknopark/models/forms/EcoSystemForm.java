package teknopark.models.forms;

import teknopark.models.entities.PhotoItem;
import teknopark.models.entities.TextItem;

public class EcoSystemForm {
    private AbstractHomeForm staticForm;

    private PhotoItem ecosystemPhotoOne;
    private  PhotoItem ecosystemPhotoTwo;

    private TextItem ecosystemHeader;
    private TextItem ecosystemDescription;
    private TextItem ecosystemItemHeader;
    private TextItem ecosystemItemOne;
    private TextItem ecosystemItemTwo;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }


    public TextItem getEcosystemHeader() {
        return ecosystemHeader;
    }

    public void setEcosystemHeader(TextItem ecosystemHeader) {
        this.ecosystemHeader = ecosystemHeader;
    }

    public TextItem getEcosystemDescription() {
        return ecosystemDescription;
    }

    public void setEcosystemDescription(TextItem ecosystemDescription) {
        this.ecosystemDescription = ecosystemDescription;
    }

    public TextItem getEcosystemItemHeader() {
        return ecosystemItemHeader;
    }

    public void setEcosystemItemHeader(TextItem ecosystemItemHeader) {
        this.ecosystemItemHeader = ecosystemItemHeader;
    }

    public PhotoItem getEcosystemPhotoOne() {
        return ecosystemPhotoOne;
    }

    public void setEcosystemPhotoOne(PhotoItem ecosystemPhotoOne) {
        this.ecosystemPhotoOne = ecosystemPhotoOne;
    }

    public PhotoItem getEcosystemPhotoTwo() {
        return ecosystemPhotoTwo;
    }

    public void setEcosystemPhotoTwo(PhotoItem ecosystemPhotoTwo) {
        this.ecosystemPhotoTwo = ecosystemPhotoTwo;
    }

    public TextItem getEcosystemItemOne() {
        return ecosystemItemOne;
    }

    public void setEcosystemItemOne(TextItem ecosystemItemOne) {
        this.ecosystemItemOne = ecosystemItemOne;
    }

    public TextItem getEcosystemItemTwo() {
        return ecosystemItemTwo;
    }

    public void setEcosystemItemTwo(TextItem ecosystemItemTwo) {
        this.ecosystemItemTwo = ecosystemItemTwo;
    }
}
