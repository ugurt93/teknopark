package teknopark.models.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "teknopark_statistics")
public class TeknoparkStatistics implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "text_id", referencedColumnName = "id")
    private TextItem text;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "color_id", referencedColumnName = "id")
    private TextItem color;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "count_id", referencedColumnName = "id")
    private TextItem count;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TextItem getText() {
        return text;
    }

    public void setText(TextItem text) {
        this.text = text;
    }

    public TextItem getColor() {
        return color;
    }

    public void setColor(TextItem color) {
        this.color = color;
    }

    public TextItem getCount() {
        return count;
    }

    public void setCount(TextItem count) {
        this.count = count;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
