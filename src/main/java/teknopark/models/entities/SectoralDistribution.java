package teknopark.models.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sectoral_distribution")
public class SectoralDistribution implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "rate_id", referencedColumnName = "id")
    private TextItem rate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "text_id", referencedColumnName = "id")
    private TextItem text;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "color_id", referencedColumnName = "id")
    private TextItem color;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "text_color_id", referencedColumnName = "id")
    private TextItem textColor;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TextItem getRate() {
        return rate;
    }

    public void setRate(TextItem rate) {
        this.rate = rate;
    }

    public TextItem getText() {
        return text;
    }

    public void setText(TextItem text) {
        this.text = text;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public TextItem getColor() {
        return color;
    }

    public void setColor(TextItem color) {
        this.color = color;
    }

    public TextItem getTextColor() {
        return textColor;
    }

    public void setTextColor(TextItem textColor) {
        this.textColor = textColor;
    }
}
