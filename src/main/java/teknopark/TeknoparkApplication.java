package teknopark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeknoparkApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeknoparkApplication.class, args);
	}
}
