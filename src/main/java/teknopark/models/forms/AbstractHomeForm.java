package teknopark.models.forms;


import teknopark.models.entities.PhotoItem;
import teknopark.models.entities.TextItem;


public class AbstractHomeForm {

    private PhotoItem headerLogo;

    private TextItem headerTeknopark;
    private TextItem headerAbout;
    private TextItem headerLegislation;
    private TextItem headerSupports;
    private TextItem headerSocialFacilities;
    private TextItem headerEcoSystem;

    private TextItem headerCommercial;
    private TextItem headerFirms;
    private TextItem headerNews;
    private TextItem headerAnnouncements;
    private TextItem headerCommunication;

    private TextItem headerFaceLink;
    private TextItem headerTwitterLink;
    private TextItem headerInstagramLink;
    private TextItem headerOnlineLogin;
    private TextItem headerAdvertisements;

    private TextItem headerContactUs;
    private TextItem headerFaq;
    private TextItem headerDirectors;
    private TextItem headerTeam;


    private PhotoItem footerPhotoTeknopark;
    private TextItem footerDescription;
    private TextItem footerConnected;
    private TextItem footerLinks;
    private TextItem footerLinkList;

    private TextItem footerTeknopark;
    private TextItem footerOurAddress;

    private PhotoItem footerAddressImage;
    private TextItem footerAddress;
    private PhotoItem footerPhoneImage;
    private TextItem footerPhone;
    private PhotoItem footerEmailImage;
    private TextItem footerEmail;

    private TextItem footerOurBulletin;
    private TextItem footerRegister;
    private TextItem footerRegisterInfo;
    private TextItem footerEmailAddress;
    private TextItem footerSend;

    private TextItem pageLang;

    public TextItem getHeaderTeknopark() {
        return headerTeknopark;
    }

    public void setHeaderTeknopark(TextItem headerTeknopark) {
        this.headerTeknopark = headerTeknopark;
    }

    public TextItem getHeaderAbout() {
        return headerAbout;
    }

    public void setHeaderAbout(TextItem headerAbout) {
        this.headerAbout = headerAbout;
    }

    public TextItem getHeaderLegislation() {
        return headerLegislation;
    }

    public void setHeaderLegislation(TextItem headerLegislation) {
        this.headerLegislation = headerLegislation;
    }

    public TextItem getHeaderSupports() {
        return headerSupports;
    }

    public void setHeaderSupports(TextItem headerSupports) {
        this.headerSupports = headerSupports;
    }

    public TextItem getHeaderSocialFacilities() {
        return headerSocialFacilities;
    }

    public void setHeaderSocialFacilities(TextItem headerSocialFacilities) {
        this.headerSocialFacilities = headerSocialFacilities;
    }

    public TextItem getHeaderEcoSystem() {
        return headerEcoSystem;
    }

    public void setHeaderEcoSystem(TextItem headerEcoSystem) {
        this.headerEcoSystem = headerEcoSystem;
    }

    public TextItem getHeaderCommercial() {
        return headerCommercial;
    }

    public void setHeaderCommercial(TextItem headerCommercial) {
        this.headerCommercial = headerCommercial;
    }

    public TextItem getHeaderFirms() {
        return headerFirms;
    }

    public void setHeaderFirms(TextItem headerFirms) {
        this.headerFirms = headerFirms;
    }

    public TextItem getHeaderNews() {
        return headerNews;
    }

    public void setHeaderNews(TextItem headerNews) {
        this.headerNews = headerNews;
    }

    public TextItem getHeaderAnnouncements() {
        return headerAnnouncements;
    }

    public void setHeaderAnnouncements(TextItem headerAnnouncements) {
        this.headerAnnouncements = headerAnnouncements;
    }

    public TextItem getHeaderCommunication() {
        return headerCommunication;
    }

    public void setHeaderCommunication(TextItem headerCommunication) {
        this.headerCommunication = headerCommunication;
    }

    public TextItem getHeaderFaceLink() {
        return headerFaceLink;
    }

    public void setHeaderFaceLink(TextItem headerFaceLink) {
        this.headerFaceLink = headerFaceLink;
    }

    public TextItem getHeaderTwitterLink() {
        return headerTwitterLink;
    }

    public void setHeaderTwitterLink(TextItem headerTwitterLink) {
        this.headerTwitterLink = headerTwitterLink;
    }

    public TextItem getHeaderInstagramLink() {
        return headerInstagramLink;
    }

    public void setHeaderInstagramLink(TextItem headerInstagramLink) {
        this.headerInstagramLink = headerInstagramLink;
    }

    public TextItem getHeaderOnlineLogin() {
        return headerOnlineLogin;
    }

    public void setHeaderOnlineLogin(TextItem headerOnlineLogin) {
        this.headerOnlineLogin = headerOnlineLogin;
    }

    public PhotoItem getFooterPhotoTeknopark() {
        return footerPhotoTeknopark;
    }

    public void setFooterPhotoTeknopark(PhotoItem footerPhotoTeknopark) {
        this.footerPhotoTeknopark = footerPhotoTeknopark;
    }

    public TextItem getFooterDescription() {
        return footerDescription;
    }

    public void setFooterDescription(TextItem footerDescription) {
        this.footerDescription = footerDescription;
    }

    public TextItem getFooterConnected() {
        return footerConnected;
    }

    public void setFooterConnected(TextItem footerConnected) {
        this.footerConnected = footerConnected;
    }

    public TextItem getFooterLinks() {
        return footerLinks;
    }

    public void setFooterLinks(TextItem footerLinks) {
        this.footerLinks = footerLinks;
    }

    public TextItem getFooterLinkList() {
        return footerLinkList;
    }

    public void setFooterLinkList(TextItem footerLinkList) {
        this.footerLinkList = footerLinkList;
    }

    public TextItem getFooterTeknopark() {
        return footerTeknopark;
    }

    public void setFooterTeknopark(TextItem footerTeknopark) {
        this.footerTeknopark = footerTeknopark;
    }

    public TextItem getFooterOurAddress() {
        return footerOurAddress;
    }

    public void setFooterOurAddress(TextItem footerOurAddress) {
        this.footerOurAddress = footerOurAddress;
    }

    public PhotoItem getFooterAddressImage() {
        return footerAddressImage;
    }

    public void setFooterAddressImage(PhotoItem footerAddressImage) {
        this.footerAddressImage = footerAddressImage;
    }

    public TextItem getFooterAddress() {
        return footerAddress;
    }

    public void setFooterAddress(TextItem footerAddress) {
        this.footerAddress = footerAddress;
    }

    public PhotoItem getFooterPhoneImage() {
        return footerPhoneImage;
    }

    public void setFooterPhoneImage(PhotoItem footerPhoneImage) {
        this.footerPhoneImage = footerPhoneImage;
    }

    public TextItem getFooterPhone() {
        return footerPhone;
    }

    public void setFooterPhone(TextItem footerPhone) {
        this.footerPhone = footerPhone;
    }

    public PhotoItem getFooterEmailImage() {
        return footerEmailImage;
    }

    public void setFooterEmailImage(PhotoItem footerEmailImage) {
        this.footerEmailImage = footerEmailImage;
    }

    public TextItem getFooterEmail() {
        return footerEmail;
    }

    public void setFooterEmail(TextItem footerEmail) {
        this.footerEmail = footerEmail;
    }

    public TextItem getFooterOurBulletin() {
        return footerOurBulletin;
    }

    public void setFooterOurBulletin(TextItem footerOurBulletin) {
        this.footerOurBulletin = footerOurBulletin;
    }

    public TextItem getFooterRegister() {
        return footerRegister;
    }

    public void setFooterRegister(TextItem footerRegister) {
        this.footerRegister = footerRegister;
    }

    public TextItem getFooterRegisterInfo() {
        return footerRegisterInfo;
    }

    public void setFooterRegisterInfo(TextItem footerRegisterInfo) {
        this.footerRegisterInfo = footerRegisterInfo;
    }

    public TextItem getFooterEmailAddress() {
        return footerEmailAddress;
    }

    public void setFooterEmailAddress(TextItem footerEmailAddress) {
        this.footerEmailAddress = footerEmailAddress;
    }

    public TextItem getFooterSend() {
        return footerSend;
    }

    public void setFooterSend(TextItem footerSend) {
        this.footerSend = footerSend;
    }

    public PhotoItem getHeaderLogo() {
        return headerLogo;
    }

    public void setHeaderLogo(PhotoItem headerLogo) {
        this.headerLogo = headerLogo;
    }

    public TextItem getHeaderAdvertisements() {
        return headerAdvertisements;
    }

    public void setHeaderAdvertisements(TextItem headerAdvertisements) {
        this.headerAdvertisements = headerAdvertisements;
    }

    public TextItem getHeaderContactUs() {
        return headerContactUs;
    }

    public void setHeaderContactUs(TextItem headerContactUs) {
        this.headerContactUs = headerContactUs;
    }

    public TextItem getHeaderFaq() {
        return headerFaq;
    }

    public void setHeaderFaq(TextItem headerFaq) {
        this.headerFaq = headerFaq;
    }

    public TextItem getHeaderDirectors() {
        return headerDirectors;
    }

    public void setHeaderDirectors(TextItem headerDirectors) {
        this.headerDirectors = headerDirectors;
    }

    public TextItem getHeaderTeam() {
        return headerTeam;
    }

    public void setHeaderTeam(TextItem headerTeam) {
        this.headerTeam = headerTeam;
    }

    public TextItem getPageLang() {
        return pageLang;
    }

    public void setPageLang(TextItem pageLang) {
        this.pageLang = pageLang;
    }
}
