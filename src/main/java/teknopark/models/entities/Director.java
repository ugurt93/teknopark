package teknopark.models.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "director")
public class Director implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "photo_id", referencedColumnName = "id")
    private PhotoItem photo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "name_id", referencedColumnName = "id")
    private TextItem name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "title_id", referencedColumnName = "id")
    private TextItem title;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PhotoItem getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoItem photo) {
        this.photo = photo;
    }

    public TextItem getName() {
        return name;
    }

    public void setName(TextItem name) {
        this.name = name;
    }

    public TextItem getTitle() {
        return title;
    }

    public void setTitle(TextItem title) {
        this.title = title;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
