package teknopark.models.forms;

import teknopark.models.entities.TextItem;

import java.util.List;

public class FirmsForm {
    private AbstractHomeForm staticForm;

    private TextItem firmHeader;
    private TextItem firmItemHeader;
    private TextItem firmCategories;

    private List<FirmElement> firms;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }

    public TextItem getFirmHeader() {
        return firmHeader;
    }

    public void setFirmHeader(TextItem firmHeader) {
        this.firmHeader = firmHeader;
    }

    public TextItem getFirmItemHeader() {
        return firmItemHeader;
    }

    public void setFirmItemHeader(TextItem firmItemHeader) {
        this.firmItemHeader = firmItemHeader;
    }

    public List<FirmElement> getFirms() {
        return firms;
    }

    public void setFirms(List<FirmElement> firms) {
        this.firms = firms;
    }

    public TextItem getFirmCategories() {
        return firmCategories;
    }

    public void setFirmCategories(TextItem firmCategories) {
        this.firmCategories = firmCategories;
    }
}
