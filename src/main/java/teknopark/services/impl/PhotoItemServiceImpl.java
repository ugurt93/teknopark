package teknopark.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import teknopark.models.entities.PhotoItem;
import teknopark.repositories.PhotoItemRepo;
import teknopark.services.PhotoItemService;

import java.util.List;

@Service
@Transactional
public class PhotoItemServiceImpl implements PhotoItemService {
    @Autowired
    private PhotoItemRepo photoItemRepo;

    @Override
    public PhotoItem findByName(String name, String lang) {
        List<PhotoItem> photoItems = photoItemRepo.findByName(name);
        if(photoItems != null && !photoItems.isEmpty()){
            for(PhotoItem photoItem : photoItems){
                photoItem.setLang(lang);
            }
            return photoItems.get(0);
        }
        return null;
    }
}
