package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.Category;

import java.util.List;

@Repository
public interface CategoryRepo extends JpaRepository<Category,Long> {
    @Query("SELECT c FROM Category c where c.isDeleted = false")
    List<Category> findAll();
}
