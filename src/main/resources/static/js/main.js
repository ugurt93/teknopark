$(document).ready(function(){

	$('#carousel-example-generic').on('slid.bs.carousel', function () {
					$(".carousel-item.active:nth-child(" + ($(".carousel-inner .carousel-item").length -1) + ") + .carousel-item").insertBefore($(".carousel-item:first-child"));
					$(".carousel-item.active:last-child").insertBefore($(".carousel-item:first-child"));
			});

			$('.slider-wrap').slick({
centerMode: true,
centerPadding: '10px',
arrows: true,
slidesToShow: 4,
nextArrow: '<i class="fas fa-angle-right fa-icon"></i>',
prevArrow: '<i class="fas fa-angle-left fa-icon"></i>',
responsive: [
{
breakpoint: 998,
settings: {
	arrows: false,
	centerMode: true,
	centerPadding: '10px',
	slidesToShow: 3
}
},
{
breakpoint: 767,
settings: {
	arrows: false,
	centerMode: true,
	centerPadding: '10px',
	slidesToShow: 2
}
},
{
breakpoint: 576,
settings: {
	arrows: false,
	centerMode: true,
	centerPadding: '10px',
	slidesToShow: 1
}
}
]
});

var card = document.querySelector('.card');
card.addEventListener( 'click', function() {
  card.classList.toggle('is-flipped');
});


});

setTimeout(visibleAdd, 100);

function visibleAdd(){
$('.event-tab-four a').each(function(){
	this.style ="display:none;";
});
}
