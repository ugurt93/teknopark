package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.Account;

import java.util.List;

@Repository
public interface AccountRepo extends JpaRepository<Account,Long> {
    @Query("SELECT a FROM Account a where a.isDeleted = false")
    List<Account> findAll();
}
