package teknopark.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import teknopark.models.entities.*;
import teknopark.models.forms.*;
import teknopark.repositories.*;
import teknopark.services.PhotoItemService;
import teknopark.services.TextItemService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {
    @Autowired
    private TextItemService textItemService;

    @Autowired
    private PhotoItemService photoItemService;

    @Autowired
    private HttpSession httpSession;

    @Autowired
    private LegislationRepo legislationRepo;

    @Autowired
    private SupportsRepo supportsRepo;

    @Autowired
    private FacilityRepo facilityRepo;

    @Autowired
    private NewsRepo newsRepo;

    @Autowired
    private AnnouncementRepo announcementRepo;

    @Autowired
    private TeamRepo teamRepo;

    @Autowired
    private FaqRepo faqRepo;

    @Autowired
    private FirmRepo firmRepo;

    @Autowired
    private CategoryRepo categoryRepo;

    @Autowired
    private SliderRepo sliderRepo;

    @Autowired
    private SectoralDistrubitionRepo sectoralDistrubitionRepo;

    @Autowired
    private StatisticsRepo statisticsRepo;

    @Autowired
    private AccountRepo accountRepo;

    private String lang= "tr";

    private AbstractHomeForm getStaticForm(String lang){
        AbstractHomeForm abstractHomeForm = new AbstractHomeForm();
        //abstractHomeForm.setHeaderLogo(photoItemService.findByName("header_logo",lang));
        abstractHomeForm.setHeaderTeknopark(textItemService.findByName("header_teknopark",lang));
        abstractHomeForm.setHeaderAbout(textItemService.findByName("header_about",lang));
        abstractHomeForm.setHeaderLegislation(textItemService.findByName("header_legislation",lang));
        abstractHomeForm.setHeaderSupports(textItemService.findByName("header_supports",lang));
        abstractHomeForm.setHeaderSocialFacilities(textItemService.findByName("header_social_facilities",lang));
        abstractHomeForm.setHeaderEcoSystem(textItemService.findByName("header_ecosystem",lang));
        abstractHomeForm.setHeaderCommercial(textItemService.findByName("header_commercial",lang));
        abstractHomeForm.setHeaderFirms(textItemService.findByName("header_firms",lang));
        abstractHomeForm.setHeaderNews(textItemService.findByName("header_news",lang));
        abstractHomeForm.setHeaderAnnouncements(textItemService.findByName("header_announcements",lang));
        abstractHomeForm.setHeaderCommunication(textItemService.findByName("header_communication",lang));
        abstractHomeForm.setHeaderFaceLink(textItemService.findByName("header_face_link",lang));
        abstractHomeForm.setHeaderTwitterLink(textItemService.findByName("header_twitter_link",lang));
        abstractHomeForm.setHeaderInstagramLink(textItemService.findByName("header_instagram_link",lang));
        abstractHomeForm.setHeaderOnlineLogin(textItemService.findByName("header_online_login",lang));
        abstractHomeForm.setHeaderAdvertisements(textItemService.findByName("header_advertisements",lang));
        abstractHomeForm.setHeaderContactUs(textItemService.findByName("header_contact_us",lang));
        abstractHomeForm.setHeaderFaq(textItemService.findByName("header_faq",lang));
        abstractHomeForm.setHeaderTeam(textItemService.findByName("header_team",lang));
        abstractHomeForm.setHeaderDirectors(textItemService.findByName("header_directors",lang));

        //abstractHomeForm.setFooterPhotoTeknopark();
        abstractHomeForm.setFooterDescription(textItemService.findByName("footer_description",lang));
        abstractHomeForm.setFooterConnected(textItemService.findByName("footer_connected",lang));
        abstractHomeForm.setFooterLinks(textItemService.findByName("footer_links",lang));
        abstractHomeForm.setFooterLinkList(textItemService.findByName("footer_linklist",lang));
        abstractHomeForm.setFooterTeknopark(textItemService.findByName("footer_teknopark",lang));
        abstractHomeForm.setFooterOurAddress(textItemService.findByName("footer_ouraddress",lang));
        //abstractHomeForm.setFooterAddressImage();
        abstractHomeForm.setFooterAddress(textItemService.findByName("footer_address",lang));
        //abstractHomeForm.setFooterAddressPhone();
        abstractHomeForm.setFooterPhone(textItemService.findByName("footer_phone",lang));
        //abstractHomeForm.setFooterAddressEmail();
        abstractHomeForm.setFooterEmail(textItemService.findByName("footer_email",lang));
        abstractHomeForm.setFooterOurBulletin(textItemService.findByName("footer_ourbulletin",lang));
        abstractHomeForm.setFooterRegister(textItemService.findByName("footer_register",lang));
        abstractHomeForm.setFooterRegisterInfo(textItemService.findByName("footer_register_info",lang));
        abstractHomeForm.setFooterEmailAddress(textItemService.findByName("footer_email_address",lang));
        abstractHomeForm.setFooterSend(textItemService.findByName("footer_send",lang));

        abstractHomeForm.setPageLang(textItemService.findByName("page_lang",lang));

        return abstractHomeForm;
    }

    @RequestMapping("/")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("index");
        IndexForm indexForm = new IndexForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        indexForm.setStaticForm(getStaticForm(lang));
        modelAndView.addObject("indexForm",indexForm);
        return modelAndView;
    }

    @RequestMapping("/hakkimizda")
    public ModelAndView hakkimizda() {
        ModelAndView modelAndView = new ModelAndView("hakkimizda");
        AboutForm aboutForm = new AboutForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        aboutForm.setStaticForm(getStaticForm(lang));

        aboutForm.setAboutAbout(textItemService.findByName("about_about",lang));
        aboutForm.setAboutDescription(textItemService.findByName("about_description",lang));
        aboutForm.setAboutText(textItemService.findByName("about_text",lang));
        aboutForm.setAboutPartner(textItemService.findByName("about_partner",lang));

        modelAndView.addObject("aboutForm",aboutForm);
        return modelAndView;

    }
    @RequestMapping("/destekler")
    public ModelAndView destekler() {

        ModelAndView modelAndView = new ModelAndView("destekler");
        SupportsForm supportsForm = new SupportsForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        supportsForm.setStaticForm(getStaticForm(lang));

        supportsForm.setSupportsHeader(textItemService.findByName("supports_header",lang));
        supportsForm.setSupportsDescription(textItemService.findByName("supports_description",lang));
        supportsForm.setSupportsItemHeader(textItemService.findByName("supports_item_header",lang));
        supportsForm.setSupportView(textItemService.findByName("supports_view",lang));

        List<Support> supports = supportsRepo.findAll();
        List<SupportElement> supportElements = new ArrayList<>();
        for(Support support : supports){
            SupportElement supportElement = new SupportElement();
            support.getHeader().setLang(lang);
            supportElement.setHeader(support.getHeader());
            support.getText().setLang(lang);
            supportElement.setText(support.getText());
            support.getColor().setLang(lang);
            supportElement.setColor(support.getColor());
            supportElements.add(supportElement);
        }
        supportsForm.setSupportElements(supportElements);

        modelAndView.addObject("supportsForm",supportsForm);
        return modelAndView;
    }

    @RequestMapping("/ekip")
    public ModelAndView ekip() {

        ModelAndView modelAndView = new ModelAndView("ekip");
        TeamForm teamForm = new TeamForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        teamForm.setStaticForm(getStaticForm(lang));

        teamForm.setTeamHeader(textItemService.findByName("team_header",lang));
        teamForm.setTeamDescription(textItemService.findByName("team_description",lang));
        teamForm.setTeamItemHeader(textItemService.findByName("team_item_header",lang));

        List<Team> teams = teamRepo.findAll();
        List<TeamElement> teamElements = new ArrayList<>();
        for(Team team : teams){
            TeamElement teamElement = new TeamElement();
            team.getName().setLang(lang);
            teamElement.setName(team.getName());
            team.getTitle().setLang(lang);
            teamElement.setTitle(team.getTitle());
            team.getPhoto().setLang(lang);
            teamElement.setPhoto(team.getPhoto());
            teamElements.add(teamElement);

        }
        teamForm.setTeams(teamElements);

        modelAndView.addObject("teamForm",teamForm);
        return modelAndView;
    }
    @RequestMapping("/ekosistem")
    public ModelAndView ekosistem() {

        ModelAndView modelAndView = new ModelAndView("ekosistem");
        EcoSystemForm ecoSystemForm = new EcoSystemForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        ecoSystemForm.setStaticForm(getStaticForm(lang));

        ecoSystemForm.setEcosystemHeader(textItemService.findByName("ecosystem_header",lang));
        ecoSystemForm.setEcosystemDescription(textItemService.findByName("ecosystem_description",lang));
        ecoSystemForm.setEcosystemItemHeader(textItemService.findByName("ecosystem_item_header",lang));
        ecoSystemForm.setEcosystemItemOne(textItemService.findByName("ecosystem_item_1",lang));
        ecoSystemForm.setEcosystemItemTwo(textItemService.findByName("ecosystem_item_2",lang));
        ecoSystemForm.setEcosystemPhotoOne(photoItemService.findByName("ecosystem_photo_1",lang));
        ecoSystemForm.setEcosystemPhotoTwo(photoItemService.findByName("ecosystem_photo_2",lang));
        modelAndView.addObject("ecoSystemForm",ecoSystemForm);
        return modelAndView;
    }

    @RequestMapping("/firmalar")
    public ModelAndView firmalar() {

        ModelAndView modelAndView = new ModelAndView("firmalar");
        FirmsForm firmsForm = new FirmsForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        firmsForm.setStaticForm(getStaticForm(lang));

        firmsForm.setFirmHeader(textItemService.findByName("firm_header",lang));
        firmsForm.setFirmItemHeader(textItemService.findByName("firm_item_header",lang));
        firmsForm.setFirmCategories(textItemService.findByName("firm_categories",lang));

        List<Firm> firms = firmRepo.findAll();
        List<FirmElement> firmElements = new ArrayList<>();
        for(Firm firm : firms){
            FirmElement firmElement = new FirmElement();
            firm.getWebsite().setLang(lang);
            firmElement.setWebsite(firm.getWebsite());

            firm.getEmail().setLang(lang);
            firmElement.setEmail(firm.getEmail());

            firm.getPhone().setLang(lang);
            firmElement.setPhone(firm.getPhone());

            firm.getCategory().setLang(lang);
            firmElement.setCategory(firm.getCategory());

            firm.getPhoto().setLang(lang);
            firmElement.setPhoto(firm.getPhoto());

            firmElements.add(firmElement);
        }

        firmsForm.setFirms(firmElements);

        List<Category> categories = categoryRepo.findAll();

        modelAndView.addObject("firmsForm",firmsForm);
        modelAndView.addObject("categories",categories);
        return modelAndView;
    }
    @RequestMapping("/haberler")
    public ModelAndView haberler() {

        ModelAndView modelAndView = new ModelAndView("haberler");
        NewsForm newsForm = new NewsForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        newsForm.setStaticForm(getStaticForm(lang));

        newsForm.setNewsHeader(textItemService.findByName("news_header",lang));
        newsForm.setNewsDescription(textItemService.findByName("news_description",lang));
        newsForm.setNewsItemHeader(textItemService.findByName("news_header",lang));
        newsForm.setNewsView(textItemService.findByName("news_view",lang));

        List<News> newss = newsRepo.findAll();
        List<NewsElement> newsElements = new ArrayList<>();
        for(News news : newss){
            NewsElement newsElement = new NewsElement();
            news.getPhoto().setLang(lang);
            newsElement.setPhoto(news.getPhoto());
            news.getText().setLang(lang);
            newsElement.setText(news.getText());
            news.getDate().setLang(lang);
            newsElement.setDate(news.getDate());
            newsElements.add(newsElement);
        }
        newsForm.setNewsElements(newsElements);

        modelAndView.addObject("newsForm",newsForm);
        return modelAndView;
    }
    @RequestMapping("/iletisim")
    public ModelAndView iletisim() {

        ModelAndView modelAndView = new ModelAndView("iletisim");
        CommunicationForm communicationForm = new CommunicationForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        communicationForm.setStaticForm(getStaticForm(lang));

        communicationForm.setContactEmail(textItemService.findByName("footer_email",lang));
        communicationForm.setContactMap(textItemService.findByName("footer_address",lang));
        communicationForm.setContactText(textItemService.findByName("footer_phone",lang));

        communicationForm.setContactPhonePhoto(photoItemService.findByName("contact_phone_photo",lang));
        communicationForm.setContactEmailPhoto(photoItemService.findByName("contact_email_photo",lang));
        communicationForm.setContactMapPhoto(photoItemService.findByName("contact_map_photo",lang));

        communicationForm.setContactTransportation(textItemService.findByName("contact_transportation",lang));


        modelAndView.addObject("communicationForm",communicationForm);
        return modelAndView;
    }
    @RequestMapping("/mevzuat")
    public ModelAndView mevzuat() {
        ModelAndView modelAndView = new ModelAndView("mevzuat");
        LegislationForm legislationForm = new LegislationForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        legislationForm.setStaticForm(getStaticForm(lang));

        legislationForm.setLegislationHeader(textItemService.findByName("legislation_header",lang));
        legislationForm.setLegislationDescription(textItemService.findByName("legislation_description",lang));
        legislationForm.setLegislationItemHeader(textItemService.findByName("legislation_item_header",lang));

        List<Legislation> legislations = legislationRepo.findAll();
        List<TextItem> legislationItems = new ArrayList<>();
        for(Legislation legislation : legislations){
            legislation.getText().setLang(lang);
            legislationItems.add(legislation.getText());

        }
        legislationForm.setLegislationItems(legislationItems);
        modelAndView.addObject("legislationForm",legislationForm);
        return modelAndView;
    }
    @RequestMapping("/sikca-sorulan-sorular")
    public ModelAndView sss() {

        ModelAndView modelAndView = new ModelAndView("sikca-sorulan-sorular");
        FaqForm faqForm = new FaqForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        faqForm.setStaticForm(getStaticForm(lang));

        faqForm.setFaqHeader(textItemService.findByName("faq_header",lang));
        faqForm.setFaqItemHeader(textItemService.findByName("faq_item_header",lang));

        List<Faq> faqs = faqRepo.findAll();
        List<FaqElement> faqElements = new ArrayList<>();
        for(Faq faq : faqs){
            FaqElement faqElement = new FaqElement();
            faq.getTitle().setLang(lang);
            faqElement.setTitle(faq.getTitle());

            faq.getText().setLang(lang);
            faqElement.setText(faq.getText());

            faq.getColor().setLang(lang);
            faqElement.setColor(faq.getColor());

            faqElement.setHeading(faq.getHeading());
            faqElement.setSection(faq.getSection());
            faqElements.add(faqElement);
        }
        faqForm.setFaqs(faqElements);

        modelAndView.addObject("faqForm",faqForm);
        return modelAndView;
    }
    @RequestMapping("/yonetim-kurulu")
    public ModelAndView yonetimkurulu() {

        ModelAndView modelAndView = new ModelAndView("yonetim-kurulu");
        DirectorsForm directorsForm = new DirectorsForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        directorsForm.setStaticForm(getStaticForm(lang));
        modelAndView.addObject("directorsForm",directorsForm);
        return modelAndView;
    }
    @RequestMapping("/sosyal-imkanlar")
    public ModelAndView sosyalimkanlar() {

        ModelAndView modelAndView = new ModelAndView("sosyal-imkanlar");
        FacilitiesForm facilitiesForm = new FacilitiesForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        facilitiesForm.setStaticForm(getStaticForm(lang));

        facilitiesForm.setFacilitiesHeader(textItemService.findByName("facilities_header",lang));
        facilitiesForm.setFacilitiesDescription(textItemService.findByName("facilities_description",lang));
        facilitiesForm.setFacilitiesItemHeader(textItemService.findByName("facilities_item_header",lang));

        List<Facility> facilities = facilityRepo.findAll();
        List<FacilityElement> facilityElements = new ArrayList<>();
        for(Facility facility : facilities){
            FacilityElement facilityElement = new FacilityElement();
            facility.getPhoto().setLang(lang);
            facilityElement.setPhoto(facility.getPhoto());
            facility.getText().setLang(lang);
            facilityElement.setText(facility.getText());
            facility.getColor().setLang(lang);
            facilityElement.setColor(facility.getColor());
            facilityElements.add(facilityElement);
        }
        facilitiesForm.setFacilityElements(facilityElements);

        modelAndView.addObject("facilitiesForm",facilitiesForm);
        return modelAndView;
    }
    @RequestMapping("/duyurular")
    public ModelAndView duyurular() {

        ModelAndView modelAndView = new ModelAndView("duyurular");
        AnnouncementForm announcementForm = new AnnouncementForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        announcementForm.setStaticForm(getStaticForm(lang));

        announcementForm.setAnnouncementHeader(textItemService.findByName("announcement_header",lang));
        announcementForm.setAnnouncementDescription(textItemService.findByName("announcement_description",lang));
        announcementForm.setAnnouncementItemHeader(textItemService.findByName("announcement_item_header",lang));

        List<Announcement> announcements = announcementRepo.findAll();
        List<AnnouncementElement> announcementElements = new ArrayList<>();
        for(Announcement announcement : announcements){
            AnnouncementElement announcementElement = new AnnouncementElement();
            announcement.getText().setLang(lang);
            announcementElement.setText(announcement.getText());
            announcement.getColor().setLang(lang);
            announcementElement.setColor(announcement.getColor());
            announcementElements.add(announcementElement);
        }
        announcementForm.setAnnouncementElements(announcementElements);

        modelAndView.addObject("announcementForm",announcementForm);
        return modelAndView;
    }
    @RequestMapping("/index")
    public ModelAndView indexx() {

        ModelAndView modelAndView = new ModelAndView("index");
        IndexForm indexForm = new IndexForm();
        if(httpSession.getAttribute("lang") != null)
            lang = (String)httpSession.getAttribute("lang");
        indexForm.setStaticForm(getStaticForm(lang));

        List<Slider> sliders = sliderRepo.findAll();
        List<SliderElement> sliderElements = new ArrayList<>();
        for(Slider slider : sliders){
            SliderElement sliderElement = new SliderElement();
            slider.getPhoto().setLang(lang);
            sliderElement.setPhoto(slider.getPhoto());

            slider.getHeader().setLang(lang);
            sliderElement.setHeader(slider.getHeader());

            slider.getText().setLang(lang);
            sliderElement.setText(slider.getText());
            sliderElements.add(sliderElement);
        }
        indexForm.setSliders(sliderElements);

        indexForm.setNewsText(textItemService.findByName("news_header",lang));
        indexForm.setNewsView(textItemService.findByName("news_view",lang));

        List<News> newss = newsRepo.findAll();
        List<NewsElement> newsElements = new ArrayList<>();
        for(News news : newss){
            NewsElement newsElement = new NewsElement();
            news.getPhoto().setLang(lang);
            newsElement.setPhoto(news.getPhoto());
            news.getText().setLang(lang);
            newsElement.setText(news.getText());
            news.getDate().setLang(lang);
            newsElement.setDate(news.getDate());
            newsElements.add(newsElement);
        }
        indexForm.setNews(newsElements);

        indexForm.setSectoralDistrubitionText(textItemService.findByName("sectoral_distrubition_header",lang));
        List<SectoralDistribution> sectoralDistributions = sectoralDistrubitionRepo.findAll();
        List<SectoralDistributionElement> sectoralDistributionElements = new ArrayList<>();
        for(SectoralDistribution sectoralDistribution : sectoralDistributions){
            SectoralDistributionElement sectoralDistributionElement = new SectoralDistributionElement();
            sectoralDistribution.getRate().setLang(lang);
            sectoralDistributionElement.setRate(sectoralDistribution.getRate());

            sectoralDistribution.getText().setLang(lang);
            sectoralDistributionElement.setText(sectoralDistribution.getText());

            sectoralDistribution.getColor().setLang(lang);
            sectoralDistributionElement.setColor(sectoralDistribution.getColor());

            sectoralDistribution.getTextColor().setLang(lang);
            sectoralDistributionElement.setTextColor(sectoralDistribution.getTextColor());

            sectoralDistributionElements.add(sectoralDistributionElement);
        }
        indexForm.setSectoralDistributions(sectoralDistributionElements);

        indexForm.setStatisticsHeader(textItemService.findByName("statistics_header",lang));
        List<TeknoparkStatistics> teknoparkStatisticses = statisticsRepo.findAll();
        List<StatisticsElement> statisticsElements = new ArrayList<>();
        for(TeknoparkStatistics teknoparkStatistics : teknoparkStatisticses){
            StatisticsElement statisticsElement = new StatisticsElement();
            teknoparkStatistics.getText().setLang(lang);
            statisticsElement.setText(teknoparkStatistics.getText());

            teknoparkStatistics.getColor().setLang(lang);
            statisticsElement.setColor(teknoparkStatistics.getColor());

            teknoparkStatistics.getCount().setLang(lang);
            statisticsElement.setCount(teknoparkStatistics.getCount());

            statisticsElements.add(statisticsElement);
        }
        indexForm.setStatistics(statisticsElements);

        indexForm.setMembersHeader(textItemService.findByName("members_header",lang));
        List<Account> accounts = accountRepo.findAll();
        List<AccountForm> accountForms = new ArrayList<>();
        for(Account account : accounts){
            AccountForm accountForm = new AccountForm();

            account.getPhoto().setLang(lang);
            accountForm.setPhoto(account.getPhoto());
            accountForms.add(accountForm);
        }
        indexForm.setAccounts(accountForms);

        indexForm.setIndexTransportation(textItemService.findByName("index_transportation",lang));

        indexForm.setActivityHeader(textItemService.findByName("activity_header",lang));

        modelAndView.addObject("indexForm",indexForm);
        return modelAndView;
    }

    @RequestMapping("/changetr")
    public ModelAndView changetr(HttpServletRequest request) {
        httpSession.setAttribute("lang", "tr");

        String referer = request.getHeader("Referer");

        return new ModelAndView("redirect:" + referer);
    }

    @RequestMapping("/changeen")
    public ModelAndView changeen(HttpServletRequest request) {
        httpSession.setAttribute("lang", "en");
        String referer = request.getHeader("Referer");
        return new ModelAndView("redirect:" + referer);
    }

}
