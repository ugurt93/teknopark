package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.Announcement;

import java.util.List;

@Repository
public interface AnnouncementRepo extends JpaRepository<Announcement,Long> {
    @Query("SELECT a FROM Announcement a where a.isDeleted = false")
    List<Announcement> findAll();
}
