package teknopark.models.forms;

import teknopark.models.entities.PhotoItem;

import java.util.List;

public class ActivityForm {
    private PhotoItem davetiyePhoto;
    private List<ActivityElement> activities;

    public PhotoItem getDavetiyePhoto() {
        return davetiyePhoto;
    }

    public void setDavetiyePhoto(PhotoItem davetiyePhoto) {
        this.davetiyePhoto = davetiyePhoto;
    }

}
