package teknopark.services;

import teknopark.models.entities.PhotoItem;

import java.util.List;

public interface PhotoItemService {

    PhotoItem findByName(String name, String lang);
}
