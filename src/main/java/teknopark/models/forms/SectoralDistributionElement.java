package teknopark.models.forms;

import teknopark.models.entities.TextItem;

public class SectoralDistributionElement {
    private TextItem text;
    private TextItem color;
    private TextItem rate;
    private TextItem textColor;

    public TextItem getText() {
        return text;
    }

    public void setText(TextItem text) {
        this.text = text;
    }

    public TextItem getColor() {
        return color;
    }

    public void setColor(TextItem color) {
        this.color = color;
    }

    public TextItem getRate() {
        return rate;
    }

    public void setRate(TextItem rate) {
        this.rate = rate;
    }

    public TextItem getTextColor() {
        return textColor;
    }

    public void setTextColor(TextItem textColor) {
        this.textColor = textColor;
    }
}
