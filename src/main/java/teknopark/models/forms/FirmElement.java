package teknopark.models.forms;

import teknopark.models.entities.Category;
import teknopark.models.entities.PhotoItem;
import teknopark.models.entities.TextItem;

public class FirmElement {
    private TextItem website;
    private TextItem email;
    private TextItem phone;
    private Category category;
    private PhotoItem photo;

    public TextItem getWebsite() {
        return website;
    }

    public void setWebsite(TextItem website) {
        this.website = website;
    }

    public TextItem getEmail() {
        return email;
    }

    public void setEmail(TextItem email) {
        this.email = email;
    }

    public TextItem getPhone() {
        return phone;
    }

    public void setPhone(TextItem phone) {
        this.phone = phone;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public PhotoItem getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoItem photo) {
        this.photo = photo;
    }
}
