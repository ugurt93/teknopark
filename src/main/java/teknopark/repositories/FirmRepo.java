package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.Firm;

import java.util.List;

@Repository
public interface FirmRepo extends JpaRepository<Firm,Long> {
    @Query("SELECT f FROM Firm f where f.isDeleted = false")
    List<Firm> findAll();
}
