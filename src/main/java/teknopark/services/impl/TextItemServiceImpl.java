package teknopark.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import teknopark.models.entities.TextItem;
import teknopark.repositories.TextItemRepo;
import teknopark.services.TextItemService;

import java.util.List;

@Service
@Transactional
public class TextItemServiceImpl implements TextItemService {
    @Autowired
    private TextItemRepo textItemRepo;

    @Override
    public TextItem findByName(String name, String lang) {
        List<TextItem> textItems = textItemRepo.findByName(name);
        if(textItems != null && !textItems.isEmpty()){
            for(TextItem textItem : textItems){
                textItem.setLang(lang);
            }
            return textItems.get(0);
        }
        return null;
    }
}
