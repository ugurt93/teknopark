package teknopark.models.forms;

import teknopark.models.entities.TextItem;

import java.util.List;

public class AnnouncementForm {
    private AbstractHomeForm staticForm;

    private List<AnnouncementElement> announcementElements;

    private TextItem announcementHeader;
    private TextItem announcementDescription;
    private TextItem announcementItemHeader;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }

    public List<AnnouncementElement> getAnnouncementElements() {
        return announcementElements;
    }

    public void setAnnouncementElements(List<AnnouncementElement> announcementElements) {
        this.announcementElements = announcementElements;
    }

    public TextItem getAnnouncementHeader() {
        return announcementHeader;
    }

    public void setAnnouncementHeader(TextItem announcementHeader) {
        this.announcementHeader = announcementHeader;
    }

    public TextItem getAnnouncementDescription() {
        return announcementDescription;
    }

    public void setAnnouncementDescription(TextItem announcementDescription) {
        this.announcementDescription = announcementDescription;
    }

    public TextItem getAnnouncementItemHeader() {
        return announcementItemHeader;
    }

    public void setAnnouncementItemHeader(TextItem announcementItemHeader) {
        this.announcementItemHeader = announcementItemHeader;
    }
}
