package teknopark.models.forms;

import teknopark.models.entities.TextItem;

public class StatisticsElement {
    private TextItem text;
    private TextItem color;
    private TextItem count;

    public TextItem getText() {
        return text;
    }

    public void setText(TextItem text) {
        this.text = text;
    }

    public TextItem getColor() {
        return color;
    }

    public void setColor(TextItem color) {
        this.color = color;
    }

    public TextItem getCount() {
        return count;
    }

    public void setCount(TextItem count) {
        this.count = count;
    }
}
