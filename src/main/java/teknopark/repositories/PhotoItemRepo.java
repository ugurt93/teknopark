package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.PhotoItem;
import teknopark.models.entities.TextItem;

import java.util.List;

@Repository
public interface PhotoItemRepo extends JpaRepository<PhotoItem,Long> {
    @Query("select t from PhotoItem t where t.isDeleted = false and t.name = :name")
    List<PhotoItem> findByName(@Param("name") String name);
}
