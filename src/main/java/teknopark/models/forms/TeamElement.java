package teknopark.models.forms;

import teknopark.models.entities.PhotoItem;
import teknopark.models.entities.TextItem;

public class TeamElement {

    private PhotoItem photo;
    private TextItem name;
    private TextItem title;

    public PhotoItem getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoItem photo) {
        this.photo = photo;
    }

    public TextItem getName() {
        return name;
    }

    public void setName(TextItem name) {
        this.name = name;
    }

    public TextItem getTitle() {
        return title;
    }

    public void setTitle(TextItem title) {
        this.title = title;
    }

}
