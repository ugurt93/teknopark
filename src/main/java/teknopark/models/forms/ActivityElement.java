package teknopark.models.forms;

import teknopark.models.entities.PhotoItem;
import teknopark.models.entities.TextItem;

public class ActivityElement {
    private PhotoItem photo;
    private TextItem date;
    private TextItem text;
    private TextItem link;

    public PhotoItem getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoItem photo) {
        this.photo = photo;
    }

    public TextItem getDate() {
        return date;
    }

    public void setDate(TextItem date) {
        this.date = date;
    }

    public TextItem getText() {
        return text;
    }

    public void setText(TextItem text) {
        this.text = text;
    }

    public TextItem getLink() {
        return link;
    }

    public void setLink(TextItem link) {
        this.link = link;
    }
}
