package teknopark.models.forms;

import teknopark.models.entities.TextItem;

public class SupportElement {
    private TextItem header;
    private TextItem text;
    private TextItem color;

    public TextItem getHeader() {
        return header;
    }

    public void setHeader(TextItem header) {
        this.header = header;
    }

    public TextItem getText() {
        return text;
    }

    public void setText(TextItem text) {
        this.text = text;
    }

    public TextItem getColor() {
        return color;
    }

    public void setColor(TextItem color) {
        this.color = color;
    }
}
