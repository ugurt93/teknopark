package teknopark.models.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "firm")
public class Firm implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "website_id", referencedColumnName = "id")
    private TextItem website;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "email_id", referencedColumnName = "id")
    private TextItem email;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "phone_id", referencedColumnName = "id")
    private TextItem phone;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "photo_id", referencedColumnName = "id")
    private PhotoItem photo;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TextItem getWebsite() {
        return website;
    }

    public void setWebsite(TextItem website) {
        this.website = website;
    }

    public TextItem getEmail() {
        return email;
    }

    public void setEmail(TextItem email) {
        this.email = email;
    }

    public TextItem getPhone() {
        return phone;
    }

    public void setPhone(TextItem phone) {
        this.phone = phone;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public PhotoItem getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoItem photo) {
        this.photo = photo;
    }
}
