package teknopark.models.forms;

import teknopark.models.entities.PhotoItem;
import teknopark.models.entities.TextItem;

public class DirectorElement {
    private PhotoItem photo;
    private TextItem text;
    private TextItem title;

    public PhotoItem getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoItem photo) {
        this.photo = photo;
    }

    public TextItem getText() {
        return text;
    }

    public void setText(TextItem text) {
        this.text = text;
    }

    public TextItem getTitle() {
        return title;
    }

    public void setTitle(TextItem title) {
        this.title = title;
    }
}
