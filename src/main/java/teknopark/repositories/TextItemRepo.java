package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.TextItem;

import java.util.List;

@Repository
public interface TextItemRepo extends JpaRepository<TextItem,Long> {

    @Query("select t from TextItem t where t.isDeleted = false and t.name = :name")
    List<TextItem> findByName(@Param("name") String name);
}
