package teknopark.models.forms;

import teknopark.models.entities.TextItem;

public class AboutForm {
    private AbstractHomeForm staticForm;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }

    private TextItem aboutAbout;
    private TextItem aboutDescription;
    private TextItem aboutText;
    private TextItem aboutPartner;

    public TextItem getAboutAbout() {
        return aboutAbout;
    }

    public void setAboutAbout(TextItem aboutAbout) {
        this.aboutAbout = aboutAbout;
    }

    public TextItem getAboutDescription() {
        return aboutDescription;
    }

    public void setAboutDescription(TextItem aboutDescription) {
        this.aboutDescription = aboutDescription;
    }

    public TextItem getAboutText() {
        return aboutText;
    }

    public void setAboutText(TextItem aboutText) {
        this.aboutText = aboutText;
    }

    public TextItem getAboutPartner() {
        return aboutPartner;
    }

    public void setAboutPartner(TextItem aboutPartner) {
        this.aboutPartner = aboutPartner;
    }
}
