package teknopark.models.forms;

import teknopark.models.entities.PhotoItem;
import teknopark.models.entities.TextItem;

public class SliderElement {
    private PhotoItem photo;
    private TextItem header;
    private TextItem text;

    public PhotoItem getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoItem photo) {
        this.photo = photo;
    }

    public TextItem getHeader() {
        return header;
    }

    public void setHeader(TextItem header) {
        this.header = header;
    }

    public TextItem getText() {
        return text;
    }

    public void setText(TextItem text) {
        this.text = text;
    }
}
