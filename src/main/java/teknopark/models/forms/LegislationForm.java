package teknopark.models.forms;

import teknopark.models.entities.TextItem;

import java.util.List;

public class LegislationForm {
    private AbstractHomeForm staticForm;

    private List<TextItem> legislationItems;

    private TextItem legislationHeader;
    private TextItem legislationDescription;
    private TextItem legislationItemHeader;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }

    public List<TextItem> getLegislationItems() {
        return legislationItems;
    }

    public void setLegislationItems(List<TextItem> legislationItems) {
        this.legislationItems = legislationItems;
    }

    public TextItem getLegislationHeader() {
        return legislationHeader;
    }

    public void setLegislationHeader(TextItem legislationHeader) {
        this.legislationHeader = legislationHeader;
    }

    public TextItem getLegislationDescription() {
        return legislationDescription;
    }

    public void setLegislationDescription(TextItem legislationDescription) {
        this.legislationDescription = legislationDescription;
    }

    public TextItem getLegislationItemHeader() {
        return legislationItemHeader;
    }

    public void setLegislationItemHeader(TextItem legislationItemHeader) {
        this.legislationItemHeader = legislationItemHeader;
    }
}
