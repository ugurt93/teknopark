package teknopark.models.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "news")
public class News implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "photo_id", referencedColumnName = "id")
    private PhotoItem photo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "date_id", referencedColumnName = "id")
    private TextItem date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "text_id", referencedColumnName = "id")
    private TextItem text;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PhotoItem getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoItem photo) {
        this.photo = photo;
    }

    public TextItem getDate() {
        return date;
    }

    public void setDate(TextItem date) {
        this.date = date;
    }

    public TextItem getText() {
        return text;
    }

    public void setText(TextItem text) {
        this.text = text;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
