package teknopark.models.forms;

import teknopark.models.entities.TextItem;

import java.util.List;

public class FacilitiesForm {
    private AbstractHomeForm staticForm;

    private TextItem facilitiesHeader;
    private TextItem facilitiesDescription;
    private TextItem facilitiesItemHeader;

    private List<FacilityElement> facilityElements;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }

    public TextItem getFacilitiesHeader() {
        return facilitiesHeader;
    }

    public void setFacilitiesHeader(TextItem facilitiesHeader) {
        this.facilitiesHeader = facilitiesHeader;
    }

    public TextItem getFacilitiesDescription() {
        return facilitiesDescription;
    }

    public void setFacilitiesDescription(TextItem facilitiesDescription) {
        this.facilitiesDescription = facilitiesDescription;
    }

    public TextItem getFacilitiesItemHeader() {
        return facilitiesItemHeader;
    }

    public void setFacilitiesItemHeader(TextItem facilitiesItemHeader) {
        this.facilitiesItemHeader = facilitiesItemHeader;
    }

    public List<FacilityElement> getFacilityElements() {
        return facilityElements;
    }

    public void setFacilityElements(List<FacilityElement> facilityElements) {
        this.facilityElements = facilityElements;
    }
}
