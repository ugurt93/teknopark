package teknopark.models.forms;

import teknopark.models.entities.TextItem;

import java.util.List;

public class IndexForm {
    private AbstractHomeForm staticForm;

    private List<SliderElement> sliders;

    private List<NewsElement> news;

    private TextItem newsText;

    private TextItem newsView;

    private TextItem sectoralDistrubitionText;

    private List<SectoralDistributionElement> sectoralDistributions;

    private List<StatisticsElement> statistics;

    private TextItem statisticsHeader;

    private TextItem membersHeader;
    private List<AccountForm> accounts;

    private TextItem indexTransportation;

    private List<ActivityForm> activityForms;

    private TextItem activityHeader;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }

    public List<SliderElement> getSliders() {
        return sliders;
    }

    public void setSliders(List<SliderElement> sliders) {
        this.sliders = sliders;
    }

    public List<NewsElement> getNews() {
        return news;
    }

    public void setNews(List<NewsElement> news) {
        this.news = news;
    }

    public TextItem getNewsText() {
        return newsText;
    }

    public void setNewsText(TextItem newsText) {
        this.newsText = newsText;
    }

    public TextItem getNewsView() {
        return newsView;
    }

    public void setNewsView(TextItem newsView) {
        this.newsView = newsView;
    }

    public TextItem getSectoralDistrubitionText() {
        return sectoralDistrubitionText;
    }

    public void setSectoralDistrubitionText(TextItem sectoralDistrubitionText) {
        this.sectoralDistrubitionText = sectoralDistrubitionText;
    }

    public List<SectoralDistributionElement> getSectoralDistributions() {
        return sectoralDistributions;
    }

    public void setSectoralDistributions(List<SectoralDistributionElement> sectoralDistributions) {
        this.sectoralDistributions = sectoralDistributions;
    }

    public List<StatisticsElement> getStatistics() {
        return statistics;
    }

    public void setStatistics(List<StatisticsElement> statistics) {
        this.statistics = statistics;
    }

    public TextItem getStatisticsHeader() {
        return statisticsHeader;
    }

    public void setStatisticsHeader(TextItem statisticsHeader) {
        this.statisticsHeader = statisticsHeader;
    }

    public TextItem getMembersHeader() {
        return membersHeader;
    }

    public void setMembersHeader(TextItem membersHeader) {
        this.membersHeader = membersHeader;
    }

    public List<AccountForm> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountForm> accounts) {
        this.accounts = accounts;
    }

    public TextItem getIndexTransportation() {
        return indexTransportation;
    }

    public void setIndexTransportation(TextItem indexTransportation) {
        this.indexTransportation = indexTransportation;
    }

    public List<ActivityForm> getActivityForms() {
        return activityForms;
    }

    public void setActivityForms(List<ActivityForm> activityForms) {
        this.activityForms = activityForms;
    }

    public TextItem getActivityHeader() {
        return activityHeader;
    }

    public void setActivityHeader(TextItem activityHeader) {
        this.activityHeader = activityHeader;
    }
}
