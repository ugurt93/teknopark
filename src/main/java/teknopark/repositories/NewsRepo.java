package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.Legislation;
import teknopark.models.entities.News;

import java.util.List;

@Repository
public interface NewsRepo extends JpaRepository<News,Long> {
    @Query("SELECT n FROM News n where n.isDeleted = false")
    List<News> findAll();
}
