package teknopark.models.forms;

import teknopark.models.entities.TextItem;

public class AnnouncementElement {
    private TextItem text;
    private TextItem color;

    public TextItem getText() {
        return text;
    }

    public void setText(TextItem text) {
        this.text = text;
    }

    public TextItem getColor() {
        return color;
    }

    public void setColor(TextItem color) {
        this.color = color;
    }
}
