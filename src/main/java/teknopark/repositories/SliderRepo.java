package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.Slider;

import java.util.List;

@Repository
public interface SliderRepo extends JpaRepository<Slider,Long> {
    @Query("SELECT s FROM Slider s where s.isDeleted = false")
    List<Slider> findAll();
}
