package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.Faq;

import java.util.List;

@Repository
public interface FaqRepo extends JpaRepository<Faq,Long> {
    @Query("SELECT f FROM Faq f where f.isDeleted = false")
    List<Faq> findAll();
}
