package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.TeknoparkStatistics;

import java.util.List;

@Repository
public interface StatisticsRepo extends JpaRepository<TeknoparkStatistics,Long> {
    @Query("SELECT s FROM TeknoparkStatistics s where s.isDeleted = false")
    List<TeknoparkStatistics> findAll();
}
