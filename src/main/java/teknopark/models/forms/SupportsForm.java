package teknopark.models.forms;

import teknopark.models.entities.TextItem;

import java.util.List;

public class SupportsForm {
    private AbstractHomeForm staticForm;

    private TextItem supportsHeader;
    private TextItem supportsDescription;
    private TextItem supportsItemHeader;

    private TextItem supportView;

    private List<SupportElement> supportElements;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }

    public TextItem getSupportsHeader() {
        return supportsHeader;
    }

    public void setSupportsHeader(TextItem supportsHeader) {
        this.supportsHeader = supportsHeader;
    }

    public TextItem getSupportsDescription() {
        return supportsDescription;
    }

    public void setSupportsDescription(TextItem supportsDescription) {
        this.supportsDescription = supportsDescription;
    }

    public List<SupportElement> getSupportElements() {
        return supportElements;
    }

    public void setSupportElements(List<SupportElement> supportElements) {
        this.supportElements = supportElements;
    }

    public TextItem getSupportsItemHeader() {
        return supportsItemHeader;
    }

    public void setSupportsItemHeader(TextItem supportsItemHeader) {
        this.supportsItemHeader = supportsItemHeader;
    }

    public TextItem getSupportView() {
        return supportView;
    }

    public void setSupportView(TextItem supportView) {
        this.supportView = supportView;
    }
}
