package teknopark.models.forms;

import teknopark.models.entities.TextItem;

import java.util.List;

public class TeamForm {
    private AbstractHomeForm staticForm;

    private List<TeamElement> teams;

    private TextItem teamHeader;
    private TextItem teamDescription;
    private TextItem teamItemHeader;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }

    public List<TeamElement> getTeams() {
        return teams;
    }

    public void setTeams(List<TeamElement> teams) {
        this.teams = teams;
    }

    public TextItem getTeamHeader() {
        return teamHeader;
    }

    public void setTeamHeader(TextItem teamHeader) {
        this.teamHeader = teamHeader;
    }

    public TextItem getTeamDescription() {
        return teamDescription;
    }

    public void setTeamDescription(TextItem teamDescription) {
        this.teamDescription = teamDescription;
    }

    public TextItem getTeamItemHeader() {
        return teamItemHeader;
    }

    public void setTeamItemHeader(TextItem teamItemHeader) {
        this.teamItemHeader = teamItemHeader;
    }
}
