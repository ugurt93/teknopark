package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import teknopark.models.entities.Team;

import java.util.List;

public interface TeamRepo extends JpaRepository<Team,Long>{
    @Query("SELECT t FROM Team t where t.isDeleted = false")
    List<Team> findAll();
}
