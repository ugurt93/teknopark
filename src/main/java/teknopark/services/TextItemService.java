package teknopark.services;

import teknopark.models.entities.TextItem;

import java.util.List;

public interface TextItemService {
    TextItem findByName(String name,String lang);
}
