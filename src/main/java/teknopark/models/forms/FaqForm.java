package teknopark.models.forms;

import teknopark.models.entities.TextItem;

import java.util.List;

public class FaqForm {
    private AbstractHomeForm staticForm;

    private TextItem faqHeader;
    private TextItem faqItemHeader;

    private List<FaqElement> faqs;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }

    public TextItem getFaqHeader() {
        return faqHeader;
    }

    public void setFaqHeader(TextItem faqHeader) {
        this.faqHeader = faqHeader;
    }

    public TextItem getFaqItemHeader() {
        return faqItemHeader;
    }

    public void setFaqItemHeader(TextItem faqItemHeader) {
        this.faqItemHeader = faqItemHeader;
    }

    public List<FaqElement> getFaqs() {
        return faqs;
    }

    public void setFaqs(List<FaqElement> faqs) {
        this.faqs = faqs;
    }
}
