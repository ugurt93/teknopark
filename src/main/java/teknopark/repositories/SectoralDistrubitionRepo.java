package teknopark.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import teknopark.models.entities.SectoralDistribution;

import java.util.List;

@Repository
public interface SectoralDistrubitionRepo extends JpaRepository<SectoralDistribution,Long> {
    @Query("SELECT s FROM SectoralDistribution s where s.isDeleted = false")
    List<SectoralDistribution> findAll();
}
