package teknopark.models.forms;

import teknopark.models.entities.PhotoItem;
import teknopark.models.entities.TextItem;

public class CommunicationForm {
    private AbstractHomeForm staticForm;

    private PhotoItem contactPhonePhoto;
    private PhotoItem contactEmailPhoto;
    private PhotoItem contactMapPhoto;

    private TextItem contactText;
    private TextItem contactEmail;
    private TextItem contactMap;

    private TextItem contactTransportation;

    public AbstractHomeForm getStaticForm() {
        return staticForm;
    }

    public void setStaticForm(AbstractHomeForm staticForm) {
        this.staticForm = staticForm;
    }

    public PhotoItem getContactPhonePhoto() {
        return contactPhonePhoto;
    }

    public void setContactPhonePhoto(PhotoItem contactPhonePhoto) {
        this.contactPhonePhoto = contactPhonePhoto;
    }

    public PhotoItem getContactEmailPhoto() {
        return contactEmailPhoto;
    }

    public void setContactEmailPhoto(PhotoItem contactEmailPhoto) {
        this.contactEmailPhoto = contactEmailPhoto;
    }

    public PhotoItem getContactMapPhoto() {
        return contactMapPhoto;
    }

    public void setContactMapPhoto(PhotoItem contactMapPhoto) {
        this.contactMapPhoto = contactMapPhoto;
    }

    public TextItem getContactText() {
        return contactText;
    }

    public void setContactText(TextItem contactText) {
        this.contactText = contactText;
    }

    public TextItem getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(TextItem contactEmail) {
        this.contactEmail = contactEmail;
    }

    public TextItem getContactMap() {
        return contactMap;
    }

    public void setContactMap(TextItem contactMap) {
        this.contactMap = contactMap;
    }

    public TextItem getContactTransportation() {
        return contactTransportation;
    }

    public void setContactTransportation(TextItem contactTransportation) {
        this.contactTransportation = contactTransportation;
    }
}
